export interface IModalInfo {
  registered: Date,
  gender: string,
  name: string,
  location: string,
  email: string,
  phone: string
}
