import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IUserResponse} from "../../interfaces/user-response.interface";

@Component({
  selector: 'app-users-male',
  templateUrl: './users-male.component.html',
  styleUrls: ['./users-male.component.scss']
})
export class UsersMaleComponent implements OnInit {

  @Input()
  maleData: any

  @Output()
  openModal: EventEmitter<any> = new EventEmitter()


  constructor() {
  }

  ngOnInit(): void {
  }

  onBtnClick(clickedUser: IUserResponse) {
    this.openModal.emit(clickedUser)
  }

}
