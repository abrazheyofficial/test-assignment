import {Component, OnInit} from '@angular/core';
import {CategoryType, UsersService} from "../../services/users.service";
import {forkJoin, Observable} from "rxjs";
import {ModalWindowService} from "../../services/modal-window.service";
import {IUserResponse} from "../../interfaces/user-response.interface";
import {IModalInfo} from "../../interfaces/modal-info.interface";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})

export class ContentComponent implements OnInit {

  public responseData: {
    male: IUserResponse[]
    female: IUserResponse[]
    all: IUserResponse[]
  } = {
    male: [],
    female: [],
    all: []
  };

  public currentQuery: string = '';
  public modalInfo: IModalInfo = {
    registered: new Date(),
    gender: '...',
    name: '...',
    location: '...',
    email: '...',
    phone: '...',
  };
  public display$!: Observable<string>;

  constructor(
    private getUsersService: UsersService,
    private modalService: ModalWindowService
  ) {
  }

  ngOnInit(): void {
    this.getUsersService.onInvalidQuery()
    this.display$ = this.modalService.watch();
    this.getDataOnParamChange()
  }

  getDataOnParamChange() {
    this.getUsersService.getParamsQuery().subscribe((gender: CategoryType) => {
      this.currentQuery = gender
      this.responseData[gender] = []

      if (gender === 'male' || 'female') {
        this.getUsersService.getUsersData(gender).subscribe((data: any) => {
          this.responseData[gender] = data.results
        })
      }

      if (gender === 'all') {
        this.responseData['male'] = []
        this.responseData['female'] = []

        forkJoin({
          maleData: this.getUsersService.getUsersData('male'),
          femaleData: this.getUsersService.getUsersData('female')
        }).subscribe((data: any) => {
          this.responseData.female = data.femaleData.results
          this.responseData.male = data.maleData.results
        })
      }

    })
  }

  showDetailedData(userInfo: IUserResponse) {
    this.modalInfo = {
      registered: new Date(userInfo.registered.date),
      gender: userInfo.gender,
      name: `${userInfo.name.first} ${userInfo.name.last}`,
      location: `${userInfo.location.country}, ${userInfo.location.city}`,
      email: userInfo.email,
      phone: userInfo.phone,
    }

    this.modalService.open()
  }

  close() {
    this.modalService.close();
  }

}
