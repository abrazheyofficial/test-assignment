import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IUserResponse} from "../../interfaces/user-response.interface";

@Component({
  selector: 'app-users-female',
  templateUrl: './users-female.component.html',
  styleUrls: ['./users-female.component.scss']
})

export class UsersFemaleComponent implements OnInit {

  @Input()
  public femaleData: IUserResponse[] = []

  @Output()
  openModal: EventEmitter<IUserResponse> = new EventEmitter()

  constructor() {
  }

  ngOnInit(): void {
  }

  onBtnClick(clickedUser: IUserResponse) {
    this.openModal.emit(clickedUser)
  }

}

