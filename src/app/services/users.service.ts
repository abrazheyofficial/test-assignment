import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {filter, map} from "rxjs/operators";
import {environment} from "../../environments/environment";

export type CategoryType = 'female' | 'male' | 'all'

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  baseUrl: string = 'https://randomuser.me/api'

  constructor(
    private activeRoute: ActivatedRoute,
    private route: Router,
    private http: HttpClient
  ) { }

  getParamsQuery() {
    return this.activeRoute.queryParams.pipe(
      map(params => params['gender']),
      filter(gender => !!gender)
    )
  }

  getUsersData(gender: CategoryType) {
    const path = `${this.baseUrl}?results=${environment.personCount}&gender=${gender}`
    return this.http.get(path)
  }

  onInvalidQuery() {
    const queryCheck = this.activeRoute.snapshot.queryParams['gender']

    if (!queryCheck || ['male', 'female', 'all'].includes(queryCheck)) {
      this.route.navigate(['/'])
    }
  }

}
